
cmake_minimum_required(VERSION 2.8.3)
project(hello_world)

## C++ 11
set ( CMAKE_CXX_STANDARD 11 )
set ( CMAKE_CXX_EXTENSIONS False )

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
 mauve_runtime
)

catkin_package(
  CATKIN_DEPENDS mauve_runtime
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_executable ( hello_world src/hello_world.cpp )
target_link_libraries ( hello_world ${catkin_LIBRARIES} )
