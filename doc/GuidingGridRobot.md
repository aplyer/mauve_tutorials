Guiding the Grid-Robot Tutorial
===============================

## Table of contents

- [Tutorial presentation](#tutorial-presentation)
- [Creating a Catkin package](#creating-a-catkin-package)
  - [Using `catkin_create_pkg`](#using-catkin_create_pkg)
  - [Using `mauve_create_pkg`](#using-mauve_create_pkg)
- [Implementing the Guidance shell](#implementing-the-guidance-shell)
  - [As a whole](#as-a-whole)
  - [Step by step](#step-by-step)
- [Declaring the Guidance Core](#declaring-the-guidance-core)
- [Defining the Guidance Core](#defining-the-guidance-core)
  - [As a whole](#as-a-whole)
  - [Step by step](#step-by-step)
- [Compiling the Guidance Core](#compiling-the-guidance-core)
- [The Guiding Architecture](#the-guiding-architecture)
  - [As a whole](#as-a-whole)
  - [Step by step](#step-by-step)
- [Create a Python Deployer](#create-a-python-deployer)
  - [Test the architecture](#test-the-architecture)
- [Solution](#solution)

## Tutorial presentation

In this tutorial we will create a *Guidance* component that send commands to the robot according to its current position, the status of its neighbor cells, and a global objective.

The objective is to implement the following architecture:

![Guiding the Robot Architecture](figures/guidingarchitecture.png)

## Creating a Catkin package

We first need to create a Catkin package that will include our developments.

To do so, you must have sourced your environment and be located in your workspace:
```bash
cd ~/mauve_ws
source devel/setup.bash
```

Then you have two options:
* if you are familiar with `catkin_create_pkg`, you can use it to create your package;
you just have to make a few modifications to compile it correctly against MAUVE Runtime;
* we provide a `mauve_create_pkg` script that creates a catkin package with minimal information to compile.

### Using `catkin_create_pkg`

Create a catkin package in `mauve_ws` that depends on `mauve_runtime` and on `simulation_tutorial` for the resource types:
```bash
cd ~/mauve_ws/src
catkin_create_pkg guiding_tutorial mauve_runtime simulation_tutorial
```
You can then update the `package.xml` file with the information you want.
You just need to activate C++11 compilation flags in your `CMakeLists.txt`.
Add the following lines in top of the file, just after the `package()` instruction:
```cmake
set ( CMAKE_CXX_STANDARD 11 )
set ( CMAKE_CXX_EXTENSIONS False )
```

### Using `mauve_create_pkg`

The `mauve_create_pkg` script is provided by the
[MAUVE Toolchain](https://gitlab.com/MAUVE/mauve_toolchain.git)
package. Follow the installation instructions of the MAUVE Toolchain.
Then you can use the `mauve_create_pkg` script either by entering the complete path
to this script, or by using `rosrun`:
```bash
cd ~/mauve_ws
mauve_create_pkg guiding_tutorial mauve_runtime simulation_tutorial -o src/guiding_tutorial
```

You can then update your `package.xml` file (see the Catkin
  [documentation](http://wiki.ros.org/catkin/package.xml)).

## Implementing the Guidance shell

The *Guidance* shell will describe the properties of the Guidance component
and its inputs and outputs.

First, create a file `GuidanceShell.hpp` in `guiding_tutorial/include/guiding_tutorial/`.

Then add the following code to this file:

### As a whole

```c++
#ifndef _GUIDING_TUTORIAL_GUIDANCESHELL_HPP_
#define _GUIDING_TUTORIAL_GUIDANCESHELL_HPP_

#include <mauve_runtime/mauve_runtime.hpp>

#include <simulation_tutorial/Command.hpp>
#include <simulation_tutorial/Pose.hpp>
#include <simulation_tutorial/Grid.hpp>

namespace guiding_tutorial {

struct GuidanceShell : public mauve::Shell {
  mauve::ReadPort<simulation_tutorial::Pose>& pose
    = mk_port< mauve::ReadPort<simulation_tutorial::Pose> >("pose",
        simulation_tutorial::Pose{ simulation_tutorial::Cell {0,0},
          simulation_tutorial::Orientation::NORTH });
  mauve::ReadPort< std::array<simulation_tutorial::Occupancy, 8> >& scan
    = mk_port< mauve::ReadPort< std::array<simulation_tutorial::Occupancy,8> > >("scan",
        std::array<simulation_tutorial::Occupancy,8>());
  mauve::ReadPort< simulation_tutorial::Cell >& target
    = mk_port< mauve::ReadPort< simulation_tutorial::Cell > >("target",
        simulation_tutorial::Cell {0, 0});
  mauve::WritePort< simulation_tutorial::Command >& command
    = mk_port< mauve::WritePort< simulation_tutorial::Command > >("command");
}; // struct GuidanceShell

} // namespace guiding_tutorial

#endif // _GUIDING_TUTORIAL_GUIDANCESHELL_HPP_
```

### Step by step

```c++
#ifndef _GUIDING_TUTORIAL_GUIDANCESHELL_HPP_
#define _GUIDING_TUTORIAL_GUIDANCESHELL_HPP_
```
protects against multiple inclusions.

```c++
#include <mauve_runtime/mauve_runtime.hpp>
```
includes the MAUVE Runtime API.

```c++
#include <simulation_tutorial/Command.hpp>
#include <simulation_tutorial/Pose.hpp>
#include <simulation_tutorial/Grid.hpp>
```
includes types defined in the Simulation tutorial.

```c++
namespace guiding_tutorial {
```
It is a good practice to define its classes into a particular namespace.

```c++
struct GuidanceShell : public mauve::Shell {
```
defines the GuidanceShell inheriting from a MAUVE Shell.

```c++
mauve::ReadPort<simulation_tutorial::Pose>& pose
  = mk_port< mauve::ReadPort<simulation_tutorial::Pose> >("pose",
      simulation_tutorial::Pose{ simulation_tutorial::Cell {0,0},
      simulation_tutorial::Orientation::NORTH });
```
creates an input port for the robot pose.

```c++
mauve::ReadPort< std::array<simulation_tutorial::Occupancy, 8> >& scan
  = mk_port< mauve::ReadPort< std::array<simulation_tutorial::Occupancy,8> > >("scan",
      std::array<simulation_tutorial::Occupancy,8>());
```
creates an input port for the laser scan.

```c++
mauve::ReadPort< simulation_tutorial::Cell >& target
  = mk_port< mauve::ReadPort< simulation_tutorial::Cell > >("target",
      simulation_tutorial::Cell {0, 0});
```
creates an input port for the target cell.

```c++
mauve::WritePort< simulation_tutorial::Command >& command
  = mk_port< mauve::WritePort< simulation_tutorial::Command > >("command");
```
creates an output port for the guidance command.

```c++
}; // struct GuidanceShell
} // namespace guiding_tutorial
#endif // _GUIDING_TUTORIAL_GUIDANCESHELL_HPP_
```
closes the Shell structure, the namespace and the `ifndef` macro.

## Declaring the Guidance Core

The *Guidance* core defines the functions of the component.

First, create a file `GuidanceCore.hpp` in `guiding_tutorial/include/guiding_tutorial/`.

Then add the following code to this file:

```c++
#ifndef GUIDING_TUTORIAL_GUIDANCECORE_HPP
#define GUIDING_TUTORIAL_GUIDANCECORE_HPP

#include <mauve_runtime/mauve_runtime.hpp>

#include "guiding_tutorial/GuidanceShell.hpp"

namespace guiding_tutorial {

  struct GuidanceCore : public mauve::Core<GuidanceShell> {
    simulation_tutorial::Cell target;
    simulation_tutorial::Command u;
    simulation_tutorial::Pose pose;
    std::array<simulation_tutorial::Occupancy, 8> scan;
    simulation_tutorial::Command compute_command();
    void command();
  }; // struct GuidanceCore

} // namespace guiding_tutorial

#endif // GUIDING_TUTORIAL_GUIDANCECORE_HPP
```
defines a *GuidanceCore* as a MAUVE Core with shell *GuidanceShell*.
The core owns a cell representing the target, the command, the pose and the scan.
It has two methods: `compute_command` computes the command from the internal pose, scan and target; `command` read the input ports, call `compute_command` and write the result.

## Defining the Guidance Core

Create a file `GuidanceCore.cpp` in `guiding_tutorial/src/` in which we will define the `command` function.

Then add the following code to this file:

### As a whole

```c++
#include "guiding_tutorial/GuidanceCore.hpp"

namespace guiding_tutorial {

void GuidanceCore::command() {
  using namespace simulation_tutorial;
  if (shell().target.get_status() == mauve::DataStatus::NEW_DATA)
    target = shell().target.read();
  scan = shell().scan.read();
  pose = shell().pose.read();
  u = compute_command();
  if (u != Command::NOOP) logger->debug("Guidance command: {}", u);
  shell().command.write(u);
}

simulation_tutorial::Command GuidanceCore::compute_command() {
  using namespace simulation_tutorial;
  Cell direction = { target.i - pose.c.i, target.j - pose.c.j };
   // Arrived
  if (direction.i == 0 && direction.j == 0)
    return Command::NOOP;

  Cell f = pose.infront();
  Cell heading = { f.i - pose.c.i, f.j - pose.c.j };
  // scalar product sign gives information about cosine, i.e. forward or backward
  int scalar_product = direction.i * heading.i + direction.j * heading.j;
  int scalar = (scalar_product == 0) ? 0 : abs(scalar_product)/scalar_product;
  // vectorial product sign gives information about sine, i.e. left or right
  int vect_product = heading.i * direction.j - direction.i * heading.j;
  int vect = (vect_product == 0) ? 0 : abs(vect_product)/vect_product;
  // weigth of adjacent cells
  std::array<int, 8> weight;
  for (int i = 0; i < 8; i++) weight[i] = static_cast<int>(scan[i]);
  weight[0] -= scalar + (1-abs(vect));
  weight[1] -= scalar + vect;
  weight[2] -= (1-abs(scalar)) + vect;
  weight[3] -= -scalar + vect;
  weight[4] -= -scalar + (1-abs(vect));
  weight[5] -= -scalar + (-vect);
  weight[6] -= (1-abs(scalar)) + (-vect);
  weight[7] -= scalar + (-vect);
  std::stringstream ss; for (auto w: weight) ss << w << ",";
  std::array<int, 8>::iterator i = std::min_element(begin(weight), end(weight));
  int o = std::distance(begin(weight), i);
  if (o == 0 || o == 1) return Command::FORWARD;
  else if (o >= 2 && o <= 4) return Command::TURN_LEFT;
  else return Command::TURN_RIGHT;
}

}
```

### Step by step

```c++
#include "guiding_tutorial/GuidanceCore.hpp"
```
includes the *GuidanceCore* header.

```c++
void GuidanceCore::command() {
  using namespace simulation_tutorial;
  if (shell().target.get_status() == mauve::DataStatus::NEW_DATA)
    target = shell().target.read();
  scan = shell().scan.read();
  pose = shell().pose.read();
  u = compute_command();
  if (u != Command::NOOP) logger->debug("Guidance command: {}", u);
  shell().command.write(u);
}
```
first reads all the input ports, then call `compute_command`. If the command is not void, the `logger` is used to display the command in *debug* level. Then the command is written to the output port.

```c++
simulation_tutorial::Command GuidanceCore::compute_command() {
  using namespace simulation_tutorial;
  Cell direction = { target.i - pose.c.i, target.j - pose.c.j };
   // Arrived
  if (direction.i == 0 && direction.j == 0)
    return Command::NOOP;

  Cell f = pose.infront();
  Cell heading = { f.i - pose.c.i, f.j - pose.c.j };
  // scalar product sign gives information about cosine, i.e. forward or backward
  int scalar_product = direction.i * heading.i + direction.j * heading.j;
  int scalar = (scalar_product == 0) ? 0 : abs(scalar_product)/scalar_product;
  // vectorial product sign gives information about sine, i.e. left or right
  int vect_product = heading.i * direction.j - direction.i * heading.j;
  int vect = (vect_product == 0) ? 0 : abs(vect_product)/vect_product;
  // weigth of adjacent cells
  std::array<int, 8> weight;
  for (int i = 0; i < 8; i++) weight[i] = static_cast<int>(scan[i]);
  weight[0] -= scalar + (1-abs(vect));
  weight[1] -= scalar + vect;
  weight[2] -= (1-abs(scalar)) + vect;
  weight[3] -= -scalar + vect;
  weight[4] -= -scalar + (1-abs(vect));
  weight[5] -= -scalar + (-vect);
  weight[6] -= (1-abs(scalar)) + (-vect);
  weight[7] -= scalar + (-vect);
  std::stringstream ss; for (auto w: weight) ss << w << ",";
  std::array<int, 8>::iterator i = std::min_element(begin(weight), end(weight));
  int o = std::distance(begin(weight), i);
  if (o == 0 || o == 1) return Command::FORWARD;
  else if (o >= 2 && o <= 4) return Command::TURN_LEFT;
  else return Command::TURN_RIGHT;
}
```
implements the guidance algorithm: each cell around the robot is given a weight, based on: (1) its occupancy (Empty=0, Obstacle=10, Unknown=2), and (2) its direction towards the target. `scalar` and `vect` are used to identify in which quadrant is the target relatively to the robot orientation.

## Compiling the Guidance Core

Edit the `CMakeLists.txt` file. Uncomment the three lines compiling the `guiding_tutorial` library:
```cmake
file ( GLOB _GUIDING_TUTORIAL_SRC src/*.cpp )
add_library ( guiding_tutorial ${_GUIDING_TUTORIAL_SRC} )
target_link_libraries ( guiding_tutorial ${catkin_LIBRARIES} )
```

Instead of using the `file` command, you can also manually set the files that you want to compile. For instance with:
```cmake
set ( _GUIDING_TUTORIAL_SRC src/GuidanceCore.cpp )
```

Moreover, you must declare your exported architectures so that the next tutorial will be able to automatically link towards them. To do so, fill the `catkin_package` macro:
```
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES guiding_tutorial
  CATKIN_DEPENDS mauve_runtime simulation_tutorial
)
```

Then build your workspace:
```bash
cd ~/mauve_ws
catkin_make
```

## The Guiding Architecture

The architecture will extend the Grid-Robot Simulation architecture from the [first tutorial](GridRobotSimulation.md), by adding a new component, *Guidance*, and a new resource attached to the target input port.

Create a file `GuidingArchitecture.hpp` in `guiding_tutorial/include/guiding_tutorial` with the following code:
### As a whole

```c++
#ifndef GUIDING_TUTORIAL_ARCHITECTURE_HPP
#define GUIDING_TUTORIAL_ARCHITECTURE_HPP

#include <mauve_runtime/mauve_runtime.hpp>
#include <simulation_tutorial/GridRobotSimulation.hpp>
#include "GuidanceShell.hpp"
#include "GuidanceCore.hpp"
#include "GuidanceFSM.hpp"

namespace guiding_tutorial {

  struct GuidingArchitecture : public simulation_tutorial::GridRobotSimulationArchitecture {

    using Guidance = mauve::Component<GuidanceShell, GuidanceCore, GuidanceFSM>;

    Guidance * guidance;

    mauve::SharedData<simulation_tutorial::Cell>* target;

    bool configure_hook() override {
      using namespace simulation_tutorial;
      using namespace mauve;

      if (!GridRobotSimulationArchitecture::configure_hook()) return false;
      // Components
      guidance = mk_component<GuidanceShell, GuidanceCore, GuidanceFSM>("guidance");
      // Resources
      target = mk_resource< SharedData<Cell> >("target");
      // Connections
      guidance->shell().target.connect(target);
      guidance->shell().pose.connect(pose);
      guidance->shell().command.connect(command);
      guidance->shell().scan.connect(scan);
      // Initial value
      target->set(Cell{ 3, 5 });
      // Configure
      return guidance->configure();
    }

  }; // GuidingArchitecture

} // namespace

#endif // GUIDING_TUTORIAL_ARCHITECTURE_HPP
```

### Step by step

```c++
#ifndef GUIDING_TUTORIAL_ARCHITECTURE_HPP
#define GUIDING_TUTORIAL_ARCHITECTURE_HPP

#include <mauve_runtime/mauve_runtime.hpp>
#include <simulation_tutorial/GridRobotSimulation.hpp>
#include "GuidanceShell.hpp"
#include "GuidanceCore.hpp"
#include "GuidanceFSM.hpp"
```
includes the necessary headers.

```c++
namespace guiding_tutorial {

  struct GuidingArchitecture : public simulation_tutorial::GridRobotSimulationArchitecture {
```
creates a new architecture, extending the *GridRobotSimulationArchitecture* from the [first tutorial](GridRobotSimulation.md), in the `guiding_tutorial` namespace.

```c++
    using Guidance = mauve::Component<GuidanceShell, GuidanceCore, GuidanceFSM>;
    Guidance * guidance;
    mauve::SharedData<simulation_tutorial::Cell>* target;
```
adds to the architecture the `guidance` component and the `target` resource.

```c++
    bool configure_hook() override {
      using namespace simulation_tutorial;
      using namespace mauve;
      if (!GridRobotSimulationArchitecture::configure_hook()) return false;
```
first configures the elements of the *GridRobotSimulationArchitecture*.

```c++
      guidance = mk_component<GuidanceShell, GuidanceCore, GuidanceFSM>("guidance");
      target = mk_resource< SharedData<Cell> >("target");
```
creates the component and the resource.

```c++
      guidance->shell().target.connect(target);
      guidance->shell().pose.connect(pose);
      guidance->shell().command.connect(command);
      guidance->shell().scan.connect(scan);
```
connects the `guidance` ports to the architectures resources.

```c++
      target->set(Cell{ 3, 5 });
      return guidance->configure();
```
put an initial value in the `target` resource and configure the `guidance` component.

```c++
    }
  }; // GuidingArchitecture
} // namespace
#endif // GUIDING_TUTORIAL_ARCHITECTURE_HPP
```
closes everything cleanly.

## Create a Python Deployer

In order to create a Python deployer of this architecture, we need to compile a simple wrapper function into a library.

In file `src/GuidingDeployer.cpp`, put:
```c++
#include <mauve_runtime/mauve_runtime.hpp>
#include "guiding_tutorial/GuidingArchitecture.hpp"

extern "C" void mk_python_deployer() {
  auto a = new guiding_tutorial::GuidingArchitecture();
  mauve::mk_deployer(a);
}
```

Then you need to either rebuild the CMake cache if you use `file(GLOB ...)` so that CMake will find the new file, of modify your `CMakeLists.txt` if you manually manage the files to compile:
```bash
cd ~/mauve_ws
catkin_make rebuild_cache
catkin_make
```

### Test the architecture

You can now launch the python interpreter, and enter the following lines to start your architecture and display debug messages of the guidance component; the robot should move from (0,0,East) to (3,5).

```python
import mauve_runtime
mauve_runtime.initialize_logger("""
default:
  level: info
  type: stdout
guidance:
  - type: stdout
    level: debug
""")
mauve_runtime.load_deployer("devel/lib/libguiding_tutorial.so")
d = mauve_runtime.deployer
d.architecture.configure()
d.create_tasks()
d.activate()
d.start()
```

## Solution

The `mauve_tutorials` repository includes the `guiding_tutorial` package completed.
You can either look at the source files to help you go on in the tutorial.
If you want to compile the solution instead of your pacakge, move the file `CATKIN_IGNORE` from the solution directory to your directory:
```bash
cd ~/mauve_ws
mv src/mauve_tutorials/guiding_tutorial/CATKIN_IGNORE src/guiding_tutorial/CATKIN_IGNORE
catkin_make
```
