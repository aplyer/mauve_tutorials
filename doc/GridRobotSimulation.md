Grid-Robot Simulation Tutorial
==============================

## Table of contents

- [Tutorial presentation](#tutorial-presentation)
- [Types](#types)
- [The Simulation resource](#the-simulation-resource)
- [The Robot component](#the-robot-component)
  - [Robot Shell](#robot-shell)
  - [Robot Core](#robot-core)
  - [Robot FSM](#robot-finite-state-machine)
  - [Configuring the component](#configuring-the-component)
- [The Laser component](#the-laser-component)
- [The Grid-Robot Simulation Architecture](#the-grid-robot-simulation-architecture)
- [The Deployer](#the-deployer)
  - [Creating a Deployer](#creating-a-deployer)
  - [Using the Python Deployer](#using-the-python-deployer)
  - [Controlling the Robot](#controlling-the-robot)
- [Logging messages](#logging-messages)


## Tutorial presentation

In this tutorial, a *Robot* is moving on a grid.

The grid managed by the simulation in this tutorial is the following:

<center>
![Grid](figures/grid.png)
</center>

The robot is initially located in cell (0,0), oriented East, i.e. facing cell (0,1). The robot is represented by a `>`.

The robot can move forward (in this example, going to cell (0,1)), turn left (resulting in being oriented North), or turn right (oriented South).

When trying to apply commands, the *Simulation* the new robot pose takes obstacles (represented by `x`) and grid limits into account.
For instance a robot at pose (0,0,North) going forward will collide with the grid limit and then stay at its position. Same thing happens for a robot at (1,1,East) facing the obstacle.

A *Laser* is also defined that returns *scans*, i.e. the status of the cells around the robot. The laser of this tutorial only see the five cells more or less in front of the robot. For the robot at pose (0,0,East) (the `>` in the table), the laser will return an array of the 8 cells around the robot being
`[.,X,X,?,?,?,.,.]`, where `.` means for *Empty*, `X` means *Obstacle*, and `?` means *Unknown* or unobserved. This array starts by the cell facing the robot pose (here (0,1)) and goes around the robot counter-clockwise.

## Types

Data types are defined in the tutorial headers:
* *Occupancy* and *Cell* in [Grid.hpp](../simulation_tutorial/include/simulation_tutorial/Grid.hpp)
* *Command* in [Command.hpp](../simulation_tutorial/include/simulation_tutorial/Command.hpp)
* *Orientation* and *Pose* in [Pose.hpp](../simulation_tutorial/include/simulation_tutorial/Pose.hpp)

## The Simulation resource

The *Simulation* is implemented as a MAUVE Resource.
A *Resource* has two main objectives: first it contains a data,
and second it provides different services to interact with its inner data.
Component ports are then connected to a resource using its corresponding services.
MAUVE Runtime provides basic resources that implement standard data-flow connections, using *SharedData* or *RingBuffer* (illustrated in the following sections).
It is also possible to define its own resource in order to provide services related to a specific structure, by inheriting from *Service*, *Resource* and *Port* classes.

The service provided by the Simulation is defined in
[Simulation.hpp](../simulation_tutorial/include/simulation_tutorial/Simulation.hpp). This service
has two functions:
* the application to a command to a pose (function *apply_command*);
* the access to the status of a call (function *get_status*).

The *SimulationPort* must implement *connection* methods (inherited from MAUVE *Port*), and give access to the *SimulationService*.

Finally, the *Simulation* resource itself is defined in class *SimulationResource*.

## The Robot component

In MAUVE, every component is defined by a Shell, a Core, and a Finite State-Machine.
The declaration of the Robot component is in
[Robot.hpp](../simulation_tutorial/include/simulation_tutorial/Robot.hpp)

### Robot Shell

The *Shell* defines the interface of a component, and contains *Properties* and *Ports*. *Ports* are used for component-to-component communication. *Properties* are only visible from the *Architecture* to configure the components.

The *Robot* shell has one property to initialize the pose of the robot, with a default value being cell (0,0), facing North.

The *Robot* shell has also three ports:
* a port to access to the Simulation resource
* an input port (*ReadPort*) in which the robot receives a *Command*
* an output port (*WritePort*) in which the robot published its *Pose*

### Robot Core

The *Core* describes the inner data and the methods of the component, that will be called from its Finite State-Machine.

The *Robot* core owns the current pose of the robot, and provides two methods:
* the *step* method that gets the input command and updates the pose accordingly;
* the *publish_pose* method that publishes the current pose to the output port.

### Robot Finite State-Machine

The *Finite State-Machine* (or *FSM*) describes the behavior of the components.
FSMs have two kind of states:
* *Execution* states are aimed at executing some code, typically by calling methods of the component core;
* *Wait* states pause the component and wait for a timer to trigger.

The *Robot* FSM defines the typical structure of a *Periodic* FSM: it has only one execution state, `E`, followed by a wait state, `W`, that is triggered at each component period.
The *Robot* execution state updates the robot pose, and then publish this new pose.

![Robot FSM](figures/robot-psm.png)

### Configuring the component

We being configured, the component will successively configure its shell, its core, and its FSM. The developer can then override the *configure_hook* methods of each element in order to specify what will happen at configuration time.

In the *Robot* component, the configuration:
* checks that the robot is connected to the Simulation (*RobotShell::configure_hook*)
* initializes the robot position from the property (*RobotCore::configure_hook*)
* defines the FSM structure (*RobotFSM::configure_hook*)

## The Laser component

The *Laser* component has a structure very similar to the *Robot* component.
See [Laser.hpp](../simulation_tutorial/include/simulation_tutorial/Laser.hpp).

The *Laser* shell has an input port with the robot pose, a simulation port, and an ouput port with the scan.
The *Laser* core owns the robot pose and defines a method to get the scan from the simulation.
The *Laser* FSM has the same Periodic FSM structure.

## The Grid-Robot Simulation Architecture

Components and resources are instantiated and connected within an *Architecture*.
The [GridRobotArchitecture](../simulation_tutorial/include/simulation_tutorial/GridRobotSimulation.hpp) instantiates:
* a *Robot* component
* a *Laser* component
* a *Simulation* resource
* *SharedData* resources for data-flow connections for command, pose and scan.

It also connects components and resources, and initialize them, by calling the *Simulation* methods and setting the *Robot* property.

The resulting architecture is shown in the following figure:

![Grid-Robot Simulation Architecture](figures/gridrobotsimulationarchitecture.png)

Components are depicted in plain rounded rectangles, resources in dashed rectangles.

## The Deployer

A *Deployer* maps the components to real-time tasks, and manage the synchronization of the tasks release times.

### Creating a Deployer

The *Deployer* for the *GridRobotArchitecture* is defined in
[GridRobotSimulationDeployer.cpp](../simulation_tutorial/src/GridRobotSimulationDeployer.cpp).
The *mauve::mk_deployer* function builds the deployer from the architecture.

The *Manager* and its *actions* are here only to help playing with the components from the *Deployer*.
Manager actions will be further explained and detailed in a future tutorial.

### Using the Python Deployer

The definition of the *mk_python_deployer* function allows to get access to this deployer from the MAUVE python binding.
For that, this function must be compiled as a shared library, which in our case is compiled as *~/mauve_ws/devel/lib/libgridrobotsimulation.so*.

First, launch a Python interpreter, for example with `ipython`.

Then enter the following commands:

```python
import mauve_runtime
```
imports the MAUVE runtime python binding.

```python
mauve_runtime.load_deployer("devel/lib/libgridrobotsimulation.so")
```
loads the library containing the *mk_python_deployer* function.

```python
d = mauve_runtime.deployer
```
get a reference to the MAUVE Deployer

```python
d.architecture.configure()
d.architecture.display()
```
configures the architecture, and displays the components and resources owned by the architecture; all the components should be in the *configured* state.

```python
d.architecture.robot.display()
```
displays the id and state of the shell, core and FSM of the *Robot* component.

```python
d.create_tasks()
d.activate()
d.start()
```
creates the tasks associated to the components, then activate them and start their execution.

Now the components are running.

```python
t_robot = d.task(d.architecture.robot)
print(t_robot)
```
references the task of the *Robot* component and displays it.

```python
print(d.architecture.pose.display())
```
prints the current content of the pose resource (written by the *Robot* component).

### Controlling the Robot

The tutorial deployer owns some *actions* to help you control the robot.
*Actions* will be detailed in a further tutorial. In this tutorial, the actions can be used to manually write a *command* in the corresponding resource, that is read by the robot.

To move the robot forward and display what happens, you can enter the following commands:
```python
print(d.architecture.simulation.display())
print(d.architecture.pose.display())
d.manager_apply("F")
print(d.architecture.pose.display())
```
This has displayed the simulation grid, and the initial pose of the robot (that should be (0,0) and East). Then it has asked the robot to move forward, and its pose is now (0,1)E.

You can play with the robot by applying actions "F" (forward), "L" (turn left) or "R" (turn right).

## Logging messages

MAUVE includes a logging mechanism based on [spdlog](https://github.com/gabime/spdlog/wiki).
The *Robot* and *Laser* components use the logger to publish messages related to their execution.

Logging can be configured from the Python deployer. For instance, to enable logging to the console, in *info* mode by default, but in *debug* mode for the *robot* component, enter the following command after having imported the `mauve_runtime` module:

```python
mauve_runtime.initialize_logger("""
default:
  level: info
  type: stdout
robot:
  - type: stdout
    level: debug
""")
```

Then restart the deployment as in [previous section](#using-the-python-deployer), and send commands: the robot logs the received command and the resulting pose.
