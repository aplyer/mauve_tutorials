MAUVE Runtime Tutorials
=======================

This section provides tutorials addressing the MAUVE Runtime API.
These tutorials must be completed successively as every tutorial relies on the previous one.
However, as a solution is provided, it is always possible to skip one section.

## Prerequisites

To complete these tutorials, you need to have installed the
[MAUVE Runtime](https://gitlab.com/MAUVE/mauve_runtime) package.

First follow the install instructions of MAUVE:
https://gitlab.com/MAUVE/mauve_runtime/blob/master/README.md

It is advised to compile the doc to be able to navigate in the MAUVE Runtime API during the tutorials:
```bash
catkin_make -DBUILD_DOC=ON
```

## Basic Tutorials

### Grid-Robot Simulation

In this first tutorial, you will look at some already written components and architectures
in order to familiarize with the Runtime API. You will also play with the python Deployer to inspect components.

[Grid-Robot Simulation Tutorial](GridRobotSimulation.md)

### Guiding the Grid-Robot

In this tutorial, you will learn how to implement a component.

[Guiding the Grid-Robot Tutorial](GuidingGridRobot.md)

### Navigating in the Grid

In this tutorial, you will learn how to write a multi-clock Finite-State-Machine.

[Navigating in the Grid Tutorial](NavigationTutorial.md)

### Mapping the Grid

In this tutorial, you will learn how to implement a resource to manage a specific data.

[Mapping the Grid Tutorial](MappingTutorial.md)
