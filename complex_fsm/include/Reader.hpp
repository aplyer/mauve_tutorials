#ifndef READER_STATUS_HPP
#define READER_STATUS_HPP


#include "mauve/runtime.hpp"
#include <iostream>

using namespace mauve::runtime;

/* --------------- Shell --------------- */

struct ReaderShell: public Shell {
  // Create the input port of type int and named "input"
  ReadPort<StatusValue<int>> & input = mk_port<ReadPort<StatusValue<int>>>("input", -1);
};

/* --------------- Core --------------- */

struct ReaderCore: public Core<ReaderShell> {

  // read and display data
  void read_data() {
    data = shell().input.read();
    std::cout << this->type_name() << "> read_data = " << data.status << " value = " << data.value << std::endl;
  }

  // First State guard
  bool first_guard() {
    return data.status == DataStatus::NEW_DATA && data.value < 5;
  }

  // First Execution
  void first_action() {
    std::cout << this->type_name() << "> first_action " << data.value << std::endl;
  }

  // Second State guard
  bool second_guard() {
    return data.status == DataStatus::NEW_DATA;
  }

  // Second action
  void second_action() {
    std::cout << this->type_name() << "> second_action " << data.value << std::endl;
  }

private:
  StatusValue<int> data;
};

/* --------------- Finite State Machine --------------- */

struct ReaderFSM: public FiniteStateMachine<ReaderShell, ReaderCore> {
  ExecState<ReaderCore>    & R = mk_execution("Read", &ReaderCore::read_data);
  ExecState<ReaderCore>    & F = mk_execution("First", &ReaderCore::first_action);
  ExecState<ReaderCore>    & S = mk_execution("Second", &ReaderCore::second_action);
  SynchroState<ReaderCore> & WR = mk_synchronization("WaitRead", ms_to_ns(250));
  SynchroState<ReaderCore> & WF = mk_synchronization("WaitFirst", ms_to_ns(500));
  SynchroState<ReaderCore> & WS = mk_synchronization("WaitSecond", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(R);

    mk_transition(R, &ReaderCore::first_guard, F);
    mk_transition(R, &ReaderCore::second_guard, S);
    set_next(R, WR);
    set_next(WR, R);

    set_next(F, WF);
    set_next(WF, R);
    set_next(S, WS);
    set_next(WS, R);
    return true;
  }
};

#endif
