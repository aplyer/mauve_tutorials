#ifndef WRITER_HPP
#define WRITER_HPP


#include "mauve/runtime.hpp"
#include <iostream>

using namespace mauve::runtime;

/* --------------- Shell --------------- */

struct WriterShell: public Shell {
  // Create the ouput port of type int and named "out"
  WritePort<int> & output = mk_port<WritePort<int>>("output");
};

/* --------------- Core --------------- */

struct WriterCore: public Core<WriterShell> {

  // Override the configure hook by setting the initial value of "count"
  bool configure_hook() override {
    count = 0;
    return true;
  }

  void update() {
    // Display current count value
    std::cout << this->type_name() << "> count = " << count << std::endl;
    // Write count to the shell port "output"
    shell().output.write(count);
    count = (count + 1) % 10;
  }

private:
  int count;
};

/* --------------- Finite State Machine --------------- */

struct WriterFSM: public FiniteStateMachine<WriterShell, WriterCore> {
  // Create the Execution state: run the update method of the core
  ExecState<WriterCore>    & U = mk_execution("Update", &WriterCore::update);
  // Create the synchronization state: 1 sec
  SynchroState<WriterCore> & W = mk_synchronization("Wait", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U); // set the initial state to Update
    set_next(U, W); // Wait after Update
    set_next(W, U); // Update after Wait
    return true;
  }
};

#endif
