#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <algorithm>

#include "libcom.hh"

namespace libcom {

bool Server::open_socket(int port) {
  int fd = socket(AF_INET, SOCK_STREAM, 0);
  if (fd < 0) return false;

  int one = 1;
  setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
  fcntl(fd, F_SETFL, O_NONBLOCK);
  struct sockaddr_in serv_addr;
  bzero((char*) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  int r = bind(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
  if (r < 0) return false;

  listen(fd, 5);
  server = fd;
  return true;
}

void Server::register_client(int fd) {
  int cli = accept(fd, nullptr, nullptr);
  if (cli < 0) return;

  fcntl(cli, F_SETFL, O_NONBLOCK);
  clients.push_back(cli);
}

bool Server::recv() {
  // fill listened sockets
  FD_ZERO(&sockets);
  FD_SET(server, &sockets);
  for (auto cli: clients)
    FD_SET(cli, &sockets);
  // select
  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec = 1000/*ms*/*500;
  int n = select(FD_SETSIZE, &sockets, (fd_set*)0, (fd_set*)0, &timeout);

  if (n <= 0) return false;

  if (FD_ISSET(server, &sockets)) // new client
    register_client(server);

  bool newdata = false;
  for (auto& cli: clients) {
    if (FD_ISSET(cli, &sockets)) {
      int r = ::recv(cli, buffer, 80, 0);
      if (r <= 0) {
	      /* Connection closed, close this end and free up entry in connectlist */
		    close(cli);
		    clients.erase(std::find(begin(clients), end(clients), cli));
	    } else {
        /* We got some data */
        newdata = true;
      }
    }
  }

  return newdata;
}

} // namespace
