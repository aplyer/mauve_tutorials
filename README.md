MAUVE Tutorials
===============

The MAUVE Tutorials package contains a bunch of tutorials for addressing all the components of the MAUVE Toolchain.

## Installing the MAUVE Tutorials

```bash
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone git@gitlab.com:mauve/mauve_tutorials.git src/mauve_tutorials
catkin_make
source devel/setup.bash
```

## Tutorials

### Runtime Tutorials

These tutorials address the MAUVE Runtime API. In this tutorial, you will learn
how to develop components, resources and architectures in the C++ API, and deploy
them in a python interpreter.

link to the [Runtime Tutorials](doc/RuntimeTutorials.md)
