#include "Writer.hpp"
#include "ReaderStatus.hpp"

struct WriterReaderStatusArchi: public Architecture {
  // Create the Writer component
  Component<WriterShell, WriterCore, WriterFSM> & writer = mk_component<WriterShell, WriterCore, WriterFSM>("writer");
  // Create the ReaderStatus component
  Component<ReaderStatusShell, ReaderStatusCore, ReaderStatusFSM> & reader = mk_component<ReaderStatusShell, ReaderStatusCore, ReaderStatusFSM>("reader");
  // Create Shared Data Resource
  SharedData<int> & data = mk_resource<SharedData<int>>("data", 0);

  bool configure_hook() override {
    // Connect the output port to the resource
    writer.shell().output.connect(data.interface().write);
    // Connect the input port to the resource
    reader.shell().input.connect(data.interface().read);

    // Configure the resource
    data.configure();
    // Configure the writer
    writer.configure();
    // Configure the reader
    reader.configure();

    // Set the affinity of the components
    // -> Both on the same processor
    writer.set_cpu(0);
    reader.set_cpu(0);

    // Set the priority of the components
    // writer is more prioritary than the reader
    writer.set_priority(10);
    reader.set_priority(11);

    return true;
  }

};

int main(int argc, char const *argv[]) {
  // Create the architecture
  WriterReaderStatusArchi architecture;

  // Create a deployer for the architecture
  AbstractDeployer* deployer = mk_abstract_deployer(&architecture);

  // Configure the architecture
  architecture.configure();

  // Create the component task
  deployer->create_tasks();
  // Activate the component task
  deployer->activate();

  // Start the deployer and the component task
  deployer->start();

  // Deployer is running until C-C
  deployer->loop();

  // Stop the deployer and the component task
  deployer->stop();

  return 0;
}
