#ifndef READER_HPP
#define READER_HPP


#include "mauve/runtime.hpp"
#include <iostream>

using namespace mauve::runtime;

/* --------------- Shell --------------- */

struct ReaderShell: public Shell {
  // Create the input port of type int and named "input"
  ReadPort<int> & input = mk_port<ReadPort<int>>("input", -1);
};

/* --------------- Core --------------- */

struct ReaderCore: public Core<ReaderShell> {

  void update() {
    // Read the input Port
    int count = shell().input.read();
    // Display current count value
    std::cout << this->type_name() << "> count = " << count << std::endl;
  }

};

/* --------------- Finite State Machine --------------- */

struct ReaderFSM: public FiniteStateMachine<ReaderShell, ReaderCore> {
  // Create the Execution state: run the update method of the core
  ExecState<ReaderCore>    & U = mk_execution("Update", &ReaderCore::update);
  // Create the synchronization state: 1 sec
  SynchroState<ReaderCore> & W = mk_synchronization("Wait", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U); // set the initial state to Update
    set_next(U, W); // Wait after Update
    set_next(W, U); // Update after Wait
    return true;
  }
};

#endif
