#ifndef MAUVE_TUTORIALS_SONAR_HPP
#define MAUVE_TUTORIALS_SONAR_HPP

#include <mauve_runtime/mauve_runtime.hpp>

#include "simulation_tutorial/Laser.hpp"

struct SonarCore : public simulation_tutorial::LaserCore {
  void grab_scan();
};

#endif
