#include "Sonar.hpp"

#include <simulation_tutorial/Grid.hpp>

void SonarCore::grab_scan() {
  using namespace simulation_tutorial;

  current_pose = shell().pose.read();
  LaserShell::scan_t s = {
    shell().simulation.get_status(current_pose.neightbor( 0)),
    Occupancy::UNKNOWN,
    Occupancy::UNKNOWN,
    Occupancy::UNKNOWN,
    Occupancy::UNKNOWN,
    Occupancy::UNKNOWN,
    Occupancy::UNKNOWN,
    Occupancy::UNKNOWN,
  };
  shell().scan.write(s);
  logger->debug("scan around {}: {}", current_pose, s);
}
